﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Conecta_4.Client;
using Conecta_4.Servidor;
using Conecta_4.BBDD;
using System.Net;
using ClienteChat;
using Conecta_4.Multiplayer;

namespace Conecta_4
{
    public partial class OnlineBoard : Form
    {
        Casilla[,] tablero;
        public int fila, col;
        public int jugador;
        public int movimientos = 0;
        private Socket sock;
        private TcpListener server = null;
        private TcpClient client;
        private BackgroundWorker MensajeRecibido = new BackgroundWorker();
        Connect_4Entities con = new Connect_4Entities();
        Score sc;
        System.Timers.Timer t;
        public int min, sec;
        private int puntuacion = 0;

        private Socket clienteSocket;
        private string nombre;
        private EndPoint epServer;
        private byte[] dataStream = new byte[1024];
        private delegate void DisplayMessageDelegate(string mensaje);
        private DisplayMessageDelegate displayMessageDelegate = null;
        
        public int getNumFilas()
        {
            return fila;
        }

        public int getNumColumnas()
        {
            return col;
        }

        public OnlineBoard(int fila, int col, bool esHost, string ip = null)
        {
            InitializeComponent();
            this.fila = fila;
            this.col = col;
            MensajeRecibido.DoWork += MensajeRecibido_DoWork;
            CheckForIllegalCrossThreadCalls = false;

            if (esHost)
            {
                jugador = 1;           
                server = new TcpListener(System.Net.IPAddress.Any, 30000);
                server.Start();
                sock = server.AcceptSocket();
            }
            else
            {
                jugador = 2;               
                client = new TcpClient(ip, 30000);
                sock = client.Client;
                MensajeRecibido.RunWorkerAsync();
            }
        }

      
        private void MensajeRecibido_DoWork(object sender, DoWorkEventArgs e)
        {
            if (jugador == 2)
                CongelarTablero();
            RecibirMovimiento(col);
            if (jugador == 1)
                DescongelarTablero();
        }

        private void CongelarTablero()
        {
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            button5.Enabled = false;
            button6.Enabled = false;
            button7.Enabled = false;
        }

        public void DescongelarTablero()
        {            
            button1.Enabled = true;
            button2.Enabled = true;          
            button3.Enabled = true;            
            button4.Enabled = true;          
            button5.Enabled = true;           
            button6.Enabled = true;          
            button7.Enabled = true;
        }

        private void RecibirMovimiento(int col)
        {
            byte[] buffer = new byte[1];
            sock.Receive(buffer);
            if (buffer[0] == 1)
            {
                pintarFicha(0);
                MovFicha();
            }
            if (buffer[0] == 2)
            {
                pintarFicha(1);
                MovFicha();
            }
            if (buffer[0] == 3)
            {
                pintarFicha(2);
                MovFicha();
            }
            if (buffer[0] == 4)
            {
                pintarFicha(3);
                MovFicha();
            }
            if (buffer[0] == 5)
            {
                pintarFicha(4);
                MovFicha();
            }
            if (buffer[0] == 6)
            {
                pintarFicha(5);
                MovFicha();
            }
            if (buffer[0] == 7)
            {
                pintarFicha(6);
                MovFicha();
            }
        }

        private void CalcularPuntuacion()
        {
            int tiempo;
            int mov = 0;

            sec *= 5;
            min *= 10;
            mov = movimientos;
            mov *= 100;
            tiempo = sec + min;
            mov -= tiempo;
            puntuacion = mov;
        }

        private void OnlineBoard_Load(object sender, EventArgs e)
        {           
            crearTablero(6, 7);
            _panelP1.Visible = false;
            _panelPuntuacion.Visible = false;
            t = new System.Timers.Timer();
            t.Start();
            t.Interval = 1000; // 1 sec
            t.Elapsed += OnTimeEvent;
            this.displayMessageDelegate = new DisplayMessageDelegate(this.DisplayMensaje);
            Server sv = new Server();

           
        }

        private void OnTimeEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            Invoke(new Action(() =>
            {
                sec += 1;
                if (sec == 60)
                {
                    sec = 0;
                    min += 1;
                }
                labelTiempo.Text = string.Format("{0}:{1}", min.ToString().PadLeft(2, '0'), sec.ToString().PadLeft(2, '0'));
            }));
        }

        private void casillaClick(object sender, EventArgs e)
        {
            int columna;
            int fila;
            int nCol, nFila;

            nCol = getNumColumnas();
            nFila = getNumFilas();

            columna = ((Casilla)sender).getCol();
            fila = ((Casilla)sender).getFila();

            if (fila <= getNumFilas())
            {
                pintarFicha(columna);
                MovFicha();
            }
            
        }
        
        private void pintarFicha(int col)
        {
            if (jugador == 1)
            {
                for (int i = 0; i < getNumFilas(); i++)
                {
                    if (tablero[i, col].vacia)
                    {
                        if (((i - 1) < getNumFilas() - 1) && (i > 0))
                        {
                            tablero[i - 1, col].Image = Properties.Resources.Vacia;
                            tablero[i - 1, col].SizeMode = PictureBoxSizeMode.StretchImage;
                        }

                        tablero[i, col].Image = Properties.Resources.fichaRoja;
                        tablero[i, col].SizeMode = PictureBoxSizeMode.StretchImage;
                        tablero[i, col].jugador = 1;
                        tablero[i, col].roja = true;
                        
                        if (i == (getNumFilas() - 1))
                        {
                            tablero[i, col].Image = Properties.Resources.fichaRoja;
                            tablero[i, col].SizeMode = PictureBoxSizeMode.StretchImage;
                            tablero[i, col].roja = true;
                            tablero[i, col].jugador = 1;
                            tablero[i, col].vacia = false;

                            if (ganar(i, col, 1) == 1)
                            {
                                _panelP1.Visible = true;
                                _panelP1.BringToFront();
                            }        
                        }
                    }
                    else if (i > 0)
                    {
                        tablero[i - 1, col].vacia = false;
                        tablero[i - 1, col].Image = Properties.Resources.fichaRoja;
                        tablero[i - 1, col].SizeMode = PictureBoxSizeMode.StretchImage;
                        tablero[i - 1, col].roja = true;
                        tablero[i - 1, col].jugador = 1;

                        if (ganar(i - 1, col, 1) == 1)
                        {
                            _panelP1.Visible = true;
                            _panelP1.BringToFront();
                        }
                        break;
                    }
                    Task.Delay(3000);
                    this.Refresh();
                }
                jugador = 2;
            }
            else
            {
                for (int i = 0; i < getNumFilas(); i++)
                {
                    if (tablero[i, col].vacia)
                    {
                        if (((i - 1) < getNumFilas() - 1) && (i > 0))
                        {
                            tablero[i - 1, col].Image = Properties.Resources.Vacia;
                            tablero[i - 1, col].SizeMode = PictureBoxSizeMode.StretchImage;
                        }

                        tablero[i, col].Image = Properties.Resources.fichaAmarilla;
                        tablero[i, col].SizeMode = PictureBoxSizeMode.StretchImage;
                        tablero[i, col].jugador = 2;
                        tablero[i, col].amarilla = true;

                        if (i == (getNumFilas() - 1))
                        {
                            tablero[i, col].vacia = false;
                            tablero[i, col].jugador = 2;
                            if (ganar(i, col, 2) == 1)
                            {
                               _panelP1.Visible = true;
                                labelP1.Text = "YOU LOST!!";
                                _panelP1.BringToFront();
                            }
                        }
                    }
                    else if (i > 0)
                    {
                        tablero[i - 1, col].vacia = false;
                        tablero[i - 1, col].jugador = 2;
                        if (ganar(i - 1, col, 2) == 1)
                        {
                            _panelP1.Visible = true;
                            labelP1.Text = "YOU LOST!!";
                            _panelP1.BringToFront();
                        }
                        break;
                    }
                    Task.Delay(3000);
                    this.Refresh();
                }
                jugador = 1;
            }
        }

        private void MovFicha()
        {
            movimientos++;
            labelNMov.Text = movimientos.ToString();
        }

        public int ganar(int fila_actual, int columna_actual, int jugador)
        {
            //bool resultado = false;
            int contador_ganar = 4;
            int contador = 0;

            //horizontal -
            //for (int c=columna_actual; c < getNumColumnas()-1; c++)
            //{
            //    if (c < 4)
            //    {
            //        if (!tablero[fila_actual, c].vacia && tablero[fila_actual, c].jugador == jugador && jugador == tablero[fila_actual, c + 1].jugador && jugador == tablero[fila_actual, c + 2].jugador && jugador == tablero[fila_actual, c + 3].jugador)
            //        {
            //            t.Stop();
            //            CalcularPuntuacion();
            //            return 1;
            //        }
            //    }
            //    else if (!tablero[fila_actual, c].vacia && tablero[fila_actual, c].jugador == jugador && jugador == tablero[fila_actual, c - 1].jugador && jugador == tablero[fila_actual, c - 2].jugador && jugador == tablero[fila_actual, c - 3].jugador)
            //    {
            //        t.Stop();
            //        CalcularPuntuacion();
            //        return 1;
            //    }
            //}
            
            //for (int f = 0; f < getNumFilas() - 1; f++)
            //{
            //    for (int c = 0; c < getNumColumnas() - 1; c++)
            //    {
            //        if (!tablero[f, c].vacia && jugador == tablero[f, c].jugador && tablero[f, c].jugador == tablero[f, c + 1].jugador && tablero[f, c].jugador == tablero[f, c + 2].jugador && tablero[f, c].jugador == tablero[f, c + 3].jugador)
            //            contador = 4;
            //        /*
            //            if (c <= getNumColumnas()-4 && !tablero[f,c].vacia && tablero[f, c].jugador == tablero[f, c + 1].jugador && tablero[f, c].jugador == tablero[f, c + 2].jugador && tablero[f, c].jugador == tablero[f, c + 3].jugador)
            //                contador=4;
                    
                   
            //            else if (!tablero[f, c].vacia && tablero[f, c].jugador == tablero[f, c - 1].jugador && tablero[f, c].jugador == tablero[f, c - 2].jugador && tablero[f, c].jugador == tablero[f, c - 3].jugador)
            //                contador=4;
            //        */
            //    }
            //    if (contador == contador_ganar)
            //    {
            //        t.Stop();
            //        return 1;
            //    }
            //}
           
            ////vertical |
            //for (int f = fila_actual; f <= getNumFilas() - 1; f++)
            //{
            //    if (!tablero[f, columna_actual].vacia && jugador == tablero[f, columna_actual].jugador)
            //    {
            //        contador++;
            //    }
            //    else
            //        break;

            //    if (contador == contador_ganar)
            //    {
            //        t.Stop();
            //        CalcularPuntuacion();
            //        return 1;
            //    }
            //}
  

            for (int f = fila_actual; f <= getNumFilas() - 1; f++)
            {
                for (int c = columna_actual; c <= getNumColumnas() - 1; c++)
                {
                    if (!tablero[f, c].vacia)
                    {     
                        if (c <= 3 && f >= 3 && jugador == tablero[f, c].jugador && jugador == tablero[f - 1, c + 1].jugador && jugador == tablero[f - 2, c + 2].jugador && jugador == tablero[f - 3, c + 3].jugador)
                            contador = 4;
                        if (c >= 3 && f <= 2 && jugador == tablero[f, c].jugador && jugador == tablero[f + 1, c - 1].jugador && jugador == tablero[f + 2, c - 2].jugador && jugador == tablero[f + 3, c - 3].jugador)
                            contador = 4;
                        else break;
                    }
                    if (contador == contador_ganar)
                    {
                        t.Stop();
                        return 1;
                    }
                }
            }

            // Descendente \
            for (int f = fila_actual; f <= getNumFilas() - 1; f++)
            {
                for (int c = columna_actual; c <= getNumColumnas() - 1; c++)
                {
                    if (!tablero[f, c].vacia)
                    {
                        if (f <= 2 && c >= 3 && jugador == tablero[f, c].jugador && jugador == tablero[f + 1, c + 1].jugador && jugador == tablero[f + 2, c + 2].jugador && jugador == tablero[f + 3, c + 3].jugador)
                            contador = 4;

                        if (c >= 3 && f >= 3 && jugador == tablero[f, c].jugador && jugador == tablero[f - 1, c - 1].jugador && jugador == tablero[f - 2, c - 2].jugador && jugador == tablero[f - 3, c - 3].jugador)
                            contador = 4;
                        else break;
                    }
                    if (contador == contador_ganar)
                    {
                        t.Stop();
                        CalcularPuntuacion();
                        return 1;
                    }
                }
            }
            //for (int f = 0; f <= getNumFilas() - 1; f++)
            //{
            //    for (int c = 0; c <= getNumColumnas() - 1; c++)
            //    {
            //        if (!tablero[f, c].vacia)
            //        {
            //            if (c >= getNumColumnas() - 4 && f >= getNumFilas() - 4 && jugador == tablero[f, c].jugador && jugador == tablero[f - 1, c - 1].jugador && jugador == tablero[f - 2, c - 2].jugador && jugador == tablero[f - 3, c - 3].jugador)
            //                contador = 4;
            //            else if (c <= getNumColumnas() - 4 && f <= getNumFilas() - 4 && jugador == tablero[f, c].jugador && jugador == tablero[f + 1, c + 1].jugador && jugador == tablero[f + 2, c + 2].jugador && jugador == tablero[f + 3, c + 3].jugador)
            //                contador = 4;
            //        }
            //        if (contador == contador_ganar)
            //        {
            //            t.Stop();
            //            return 1;
            //        }
            //    }
            //}
            return 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            byte[] num = { 1 };
            sock.Send(num);
            pintarFicha(0);
            MovFicha();
            MensajeRecibido.RunWorkerAsync();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            byte[] num = { 2 };
            sock.Send(num);
            pintarFicha(1);
            MovFicha();
            MensajeRecibido.RunWorkerAsync();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            byte[] num = { 3 };
            sock.Send(num);
            pintarFicha(2);
            MovFicha();
            MensajeRecibido.RunWorkerAsync();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            byte[] num = { 4 };
            sock.Send(num);
            pintarFicha(3);
            MovFicha();
            MensajeRecibido.RunWorkerAsync();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            byte[] num = { 5 };
            sock.Send(num);
            pintarFicha(4);
            MovFicha();
            MensajeRecibido.RunWorkerAsync();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            byte[] num = { 6 };
            sock.Send(num);
            pintarFicha(5);
            MovFicha();
            MensajeRecibido.RunWorkerAsync();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            byte[] num = { 7 };
            sock.Send(num);
            pintarFicha(6);
            MovFicha();
            MensajeRecibido.RunWorkerAsync();
        }

        public void crearTablero(int fila, int col)
        {
            tablero = new Casilla[fila, col];
            for (int f = 0; f < fila; f++)
                for (int c = 0; c < col; c++)
                {
                    tablero[f, c] = new Casilla(panelTablero);
                    tablero[f, c].setFila(f);
                    tablero[f, c].setCol(c);
                    tablero[f, c].posicion(90 * f, 90 * c);
                    tablero[f, c].dimension(90, 90);
                    tablero[f, c].Image = Properties.Resources.Vacia;
                    tablero[f, c].SizeMode = PictureBoxSizeMode.StretchImage;
                    tablero[f, c].Visible = true;
                    tablero[f, c].BringToFront();
                    //tablero[f, c].Click += casillaClick;
                }
            this.Refresh();
            
        }

        private void buttonPuntuacionP1_Click(object sender, EventArgs e)
        {
            _panelP1.Visible = false;
            _panelPuntuacion.Visible = true;
            _panelPuntuacion.BringToFront();
            string nombre = textBoxNombreP1.Text;
            labelNombre.Text = nombre;
            string mov = Convert.ToString(movimientos);
            labelMoves.Text = mov;
            label10.Text = labelTiempo.Text;
            labelPuntos.Text = puntuacion + " Pts";

            sc = new Score();
            sc.Nombre_jugador = textBoxNombreP1.Text;
            sc.Movimiento_ficha = movimientos;
            sc.Puntuacion = labelPuntos.Text;
            sc.Tiempo_invertido = TimeSpan.Parse(labelTiempo.Text);

            int resultado = ScoreDAL.Agregar(sc);
            
            
        }



        private void btnHome_Click_1(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Visible = true;
            Dispose();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            _panelPuntuacion.Visible = false;
            _panelP1.Visible = false;
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                Paquete enviarDato = new Paquete();
                enviarDato.NombreChat = this.nombre;
                enviarDato.MensajeChat = textoMensaje.Text.Trim();
                enviarDato.ChatDataID = DataID.Mensaje;

                byte[] byteDato = enviarDato.GetDataStream();

                clienteSocket.BeginSendTo(byteDato, 0, byteDato.Length, SocketFlags.None, epServer, new AsyncCallback(this.EnviarDato), null);

                textoMensaje.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al enviar: " + ex.Message, "Cliente UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void botonConectar_Click(object sender, EventArgs e)
        {
            try
            {
                this.nombre = txtNombre.Text.Trim();

                Paquete enviarDato = new Paquete();
                enviarDato.NombreChat = this.nombre;
                enviarDato.MensajeChat = null;
                enviarDato.ChatDataID = DataID.LogIn;

                this.clienteSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Tcp);

                IPAddress serverIP = IPAddress.Parse("127.0.0.1");

                IPEndPoint server = new IPEndPoint(serverIP, 30000);

                epServer = (EndPoint)server;

                byte[] dato = enviarDato.GetDataStream();

                clienteSocket.BeginSendTo(dato, 0, dato.Length, SocketFlags.None, epServer, new AsyncCallback(this.EnviarDato), null);

                this.dataStream = new byte[1024];

                clienteSocket.BeginReceiveFrom(this.dataStream, 0, this.dataStream.Length, SocketFlags.None, ref epServer, new AsyncCallback(this.RecibirDato), null);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de conexión: " + ex.Message, "Cliente UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void EnviarDato(IAsyncResult ar)
        {
            try
            {
                clienteSocket.EndSend(ar);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Dato enviado: " + ex.Message, "Cliente UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RecibirDato(IAsyncResult ar)
        {
            try
            {
                this.clienteSocket.EndReceive(ar);

                Paquete recibirDato = new Paquete(this.dataStream);

                if (recibirDato.MensajeChat != null)
                    this.Invoke(this.displayMessageDelegate, new object[] { recibirDato.MensajeChat });

                this.dataStream = new byte[1024];

                clienteSocket.BeginReceiveFrom(this.dataStream, 0, this.dataStream.Length, SocketFlags.None, ref epServer, new AsyncCallback(this.RecibirDato), null);
            }
            catch (ObjectDisposedException)
            { }
            catch (Exception ex)
            {
                MessageBox.Show("Dato recibido: " + ex.Message, "Cliente UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DisplayMensaje(string mensaje)
        {
            textoChat.Text += mensaje + Environment.NewLine;
        }

        private void sERVIDORToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Server sv = new Server();
            sv.Show();
        }

        private void OnlineBoard_FormClosing (object sender, FormClosingEventArgs e)
        {
            MensajeRecibido.WorkerSupportsCancellation = true;
            MensajeRecibido.CancelAsync();
            if (server != null)
                server.Stop();
            try
            {
                if (this.clienteSocket != null)
                {
                    Paquete enviarDato = new Paquete();
                    enviarDato.ChatDataID = DataID.LogOut;
                    enviarDato.NombreChat = this.nombre;
                    enviarDato.MensajeChat = null;

                    byte[] byteDato = enviarDato.GetDataStream();

                    this.clienteSocket.SendTo(byteDato, 0, byteDato.Length, SocketFlags.None, epServer);

                    this.clienteSocket.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al cerrar: " + ex.Message, "Cliente UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
