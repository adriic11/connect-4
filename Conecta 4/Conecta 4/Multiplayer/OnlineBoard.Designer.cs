﻿namespace Conecta_4
{
    partial class OnlineBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTablero = new System.Windows.Forms.Panel();
            this._panelP1 = new System.Windows.Forms.Panel();
            this.panelP1 = new System.Windows.Forms.Panel();
            this.buttonPuntuacionP1 = new System.Windows.Forms.Button();
            this.textBoxNombreP1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelP1 = new System.Windows.Forms.Label();
            this._panelPuntuacion = new System.Windows.Forms.Panel();
            this.panelPuntuacion = new System.Windows.Forms.Panel();
            this.labelPuntos = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelMoves = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnNewGame = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelNombre = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.labelMov = new System.Windows.Forms.Label();
            this.labelNMov = new System.Windows.Forms.Label();
            this.labelTiempo = new System.Windows.Forms.Label();
            this._labelTiempo = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textoMensaje = new System.Windows.Forms.TextBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.botonConectar = new System.Windows.Forms.Button();
            this.textoChat = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.sERVIDORToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelTablero.SuspendLayout();
            this._panelP1.SuspendLayout();
            this.panelP1.SuspendLayout();
            this._panelPuntuacion.SuspendLayout();
            this.panelPuntuacion.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTablero
            // 
            this.panelTablero.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelTablero.Controls.Add(this._panelP1);
            this.panelTablero.Controls.Add(this._panelPuntuacion);
            this.panelTablero.Cursor = System.Windows.Forms.Cursors.Default;
            this.panelTablero.Location = new System.Drawing.Point(95, 125);
            this.panelTablero.Name = "panelTablero";
            this.panelTablero.Size = new System.Drawing.Size(692, 567);
            this.panelTablero.TabIndex = 0;
            // 
            // _panelP1
            // 
            this._panelP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._panelP1.Controls.Add(this.panelP1);
            this._panelP1.Location = new System.Drawing.Point(427, 88);
            this._panelP1.Name = "_panelP1";
            this._panelP1.Size = new System.Drawing.Size(177, 205);
            this._panelP1.TabIndex = 270;
            // 
            // panelP1
            // 
            this.panelP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelP1.Controls.Add(this.buttonPuntuacionP1);
            this.panelP1.Controls.Add(this.textBoxNombreP1);
            this.panelP1.Controls.Add(this.label1);
            this.panelP1.Controls.Add(this.labelP1);
            this.panelP1.Location = new System.Drawing.Point(3, 3);
            this.panelP1.Name = "panelP1";
            this.panelP1.Size = new System.Drawing.Size(171, 199);
            this.panelP1.TabIndex = 94;
            // 
            // buttonPuntuacionP1
            // 
            this.buttonPuntuacionP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.buttonPuntuacionP1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPuntuacionP1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.buttonPuntuacionP1.Location = new System.Drawing.Point(48, 143);
            this.buttonPuntuacionP1.Name = "buttonPuntuacionP1";
            this.buttonPuntuacionP1.Size = new System.Drawing.Size(75, 23);
            this.buttonPuntuacionP1.TabIndex = 3;
            this.buttonPuntuacionP1.Text = "Puntuación";
            this.buttonPuntuacionP1.UseVisualStyleBackColor = false;
            this.buttonPuntuacionP1.Click += new System.EventHandler(this.buttonPuntuacionP1_Click);
            // 
            // textBoxNombreP1
            // 
            this.textBoxNombreP1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxNombreP1.Location = new System.Drawing.Point(37, 96);
            this.textBoxNombreP1.Name = "textBoxNombreP1";
            this.textBoxNombreP1.Size = new System.Drawing.Size(95, 20);
            this.textBoxNombreP1.TabIndex = 2;
            this.textBoxNombreP1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(28, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Introduzca su nombre :";
            // 
            // labelP1
            // 
            this.labelP1.AutoSize = true;
            this.labelP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP1.ForeColor = System.Drawing.Color.White;
            this.labelP1.Location = new System.Drawing.Point(44, 15);
            this.labelP1.Name = "labelP1";
            this.labelP1.Size = new System.Drawing.Size(88, 18);
            this.labelP1.TabIndex = 0;
            this.labelP1.Text = "YOU WIN!!";
            // 
            // _panelPuntuacion
            // 
            this._panelPuntuacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this._panelPuntuacion.Controls.Add(this.panelPuntuacion);
            this._panelPuntuacion.Location = new System.Drawing.Point(195, 88);
            this._panelPuntuacion.Name = "_panelPuntuacion";
            this._panelPuntuacion.Size = new System.Drawing.Size(207, 327);
            this._panelPuntuacion.TabIndex = 95;
            // 
            // panelPuntuacion
            // 
            this.panelPuntuacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelPuntuacion.Controls.Add(this.labelPuntos);
            this.panelPuntuacion.Controls.Add(this.label10);
            this.panelPuntuacion.Controls.Add(this.labelMoves);
            this.panelPuntuacion.Controls.Add(this.label8);
            this.panelPuntuacion.Controls.Add(this.btnNewGame);
            this.panelPuntuacion.Controls.Add(this.btnHome);
            this.panelPuntuacion.Controls.Add(this.btnBack);
            this.panelPuntuacion.Controls.Add(this.label7);
            this.panelPuntuacion.Controls.Add(this.label6);
            this.panelPuntuacion.Controls.Add(this.labelNombre);
            this.panelPuntuacion.Location = new System.Drawing.Point(3, 4);
            this.panelPuntuacion.Name = "panelPuntuacion";
            this.panelPuntuacion.Size = new System.Drawing.Size(200, 320);
            this.panelPuntuacion.TabIndex = 93;
            // 
            // labelPuntos
            // 
            this.labelPuntos.AutoSize = true;
            this.labelPuntos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPuntos.ForeColor = System.Drawing.Color.Blue;
            this.labelPuntos.Location = new System.Drawing.Point(78, 154);
            this.labelPuntos.Name = "labelPuntos";
            this.labelPuntos.Size = new System.Drawing.Size(44, 13);
            this.labelPuntos.TabIndex = 13;
            this.labelPuntos.Text = "Moves";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(139, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Moves";
            // 
            // labelMoves
            // 
            this.labelMoves.AutoSize = true;
            this.labelMoves.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMoves.ForeColor = System.Drawing.Color.Blue;
            this.labelMoves.Location = new System.Drawing.Point(30, 86);
            this.labelMoves.Name = "labelMoves";
            this.labelMoves.Size = new System.Drawing.Size(44, 13);
            this.labelMoves.TabIndex = 11;
            this.labelMoves.Text = "Moves";
            this.labelMoves.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(62, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 16);
            this.label8.TabIndex = 10;
            this.label8.Text = "Puntuación";
            // 
            // btnNewGame
            // 
            this.btnNewGame.BackColor = System.Drawing.Color.Blue;
            this.btnNewGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewGame.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnNewGame.Location = new System.Drawing.Point(65, 231);
            this.btnNewGame.Name = "btnNewGame";
            this.btnNewGame.Size = new System.Drawing.Size(75, 23);
            this.btnNewGame.TabIndex = 9;
            this.btnNewGame.Text = "New Game";
            this.btnNewGame.UseVisualStyleBackColor = false;
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.Blue;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnHome.Location = new System.Drawing.Point(65, 266);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(75, 23);
            this.btnHome.TabIndex = 8;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click_1);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.Blue;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(65, 195);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 7;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(123, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Tiempo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(15, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Movimientos";
            // 
            // labelNombre
            // 
            this.labelNombre.AutoSize = true;
            this.labelNombre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNombre.ForeColor = System.Drawing.Color.White;
            this.labelNombre.Location = new System.Drawing.Point(62, 22);
            this.labelNombre.Name = "labelNombre";
            this.labelNombre.Size = new System.Drawing.Size(85, 20);
            this.labelNombre.TabIndex = 4;
            this.labelNombre.Text = "NOMBRE";
            this.labelNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(105, 100);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 33);
            this.button1.TabIndex = 263;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(194, 100);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(81, 33);
            this.button2.TabIndex = 264;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Location = new System.Drawing.Point(281, 99);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(81, 33);
            this.button3.TabIndex = 265;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Location = new System.Drawing.Point(368, 99);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(81, 33);
            this.button4.TabIndex = 266;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(456, 99);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(81, 33);
            this.button5.TabIndex = 267;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Location = new System.Drawing.Point(546, 100);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(81, 33);
            this.button6.TabIndex = 268;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Location = new System.Drawing.Point(634, 100);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(81, 33);
            this.button7.TabIndex = 269;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // labelMov
            // 
            this.labelMov.AutoSize = true;
            this.labelMov.Font = new System.Drawing.Font("Mongolian Baiti", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMov.Location = new System.Drawing.Point(171, 36);
            this.labelMov.Name = "labelMov";
            this.labelMov.Size = new System.Drawing.Size(142, 23);
            this.labelMov.TabIndex = 271;
            this.labelMov.Text = "Movimientos:";
            // 
            // labelNMov
            // 
            this.labelNMov.AutoSize = true;
            this.labelNMov.Font = new System.Drawing.Font("Yu Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNMov.Location = new System.Drawing.Point(315, 31);
            this.labelNMov.Name = "labelNMov";
            this.labelNMov.Size = new System.Drawing.Size(28, 31);
            this.labelNMov.TabIndex = 272;
            this.labelNMov.Text = "0";
            // 
            // labelTiempo
            // 
            this.labelTiempo.AutoSize = true;
            this.labelTiempo.Font = new System.Drawing.Font("Yu Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTiempo.Location = new System.Drawing.Point(538, 30);
            this.labelTiempo.Name = "labelTiempo";
            this.labelTiempo.Size = new System.Drawing.Size(83, 35);
            this.labelTiempo.TabIndex = 273;
            this.labelTiempo.Text = "00:00";
            // 
            // _labelTiempo
            // 
            this._labelTiempo.AutoSize = true;
            this._labelTiempo.Font = new System.Drawing.Font("Mongolian Baiti", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._labelTiempo.Location = new System.Drawing.Point(448, 35);
            this._labelTiempo.Name = "_labelTiempo";
            this._labelTiempo.Size = new System.Drawing.Size(89, 23);
            this._labelTiempo.TabIndex = 274;
            this._labelTiempo.Text = "Tiempo:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textoMensaje);
            this.groupBox2.Controls.Add(this.btnEnviar);
            this.groupBox2.Location = new System.Drawing.Point(891, 441);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 90);
            this.groupBox2.TabIndex = 277;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mensaje";
            // 
            // textoMensaje
            // 
            this.textoMensaje.Location = new System.Drawing.Point(8, 22);
            this.textoMensaje.Multiline = true;
            this.textoMensaje.Name = "textoMensaje";
            this.textoMensaje.Size = new System.Drawing.Size(262, 57);
            this.textoMensaje.TabIndex = 0;
            // 
            // btnEnviar
            // 
            this.btnEnviar.Location = new System.Drawing.Point(276, 22);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(86, 57);
            this.btnEnviar.TabIndex = 1;
            this.btnEnviar.Text = "En&viar";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(918, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nombre:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(971, 120);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(150, 20);
            this.txtNombre.TabIndex = 1;
            // 
            // botonConectar
            // 
            this.botonConectar.Location = new System.Drawing.Point(1141, 106);
            this.botonConectar.Name = "botonConectar";
            this.botonConectar.Size = new System.Drawing.Size(131, 46);
            this.botonConectar.TabIndex = 4;
            this.botonConectar.Text = "Con&ectar";
            this.botonConectar.UseVisualStyleBackColor = true;
            this.botonConectar.Click += new System.EventHandler(this.botonConectar_Click);
            // 
            // textoChat
            // 
            this.textoChat.Location = new System.Drawing.Point(891, 167);
            this.textoChat.Name = "textoChat";
            this.textoChat.ReadOnly = true;
            this.textoChat.Size = new System.Drawing.Size(368, 268);
            this.textoChat.TabIndex = 276;
            this.textoChat.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sERVIDORToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1370, 24);
            this.menuStrip1.TabIndex = 279;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // sERVIDORToolStripMenuItem
            // 
            this.sERVIDORToolStripMenuItem.Name = "sERVIDORToolStripMenuItem";
            this.sERVIDORToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.sERVIDORToolStripMenuItem.Text = "sERVIDOR";
            this.sERVIDORToolStripMenuItem.Click += new System.EventHandler(this.sERVIDORToolStripMenuItem_Click);
            // 
            // OnlineBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ClientSize = new System.Drawing.Size(1370, 869);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.botonConectar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.textoChat);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this._labelTiempo);
            this.Controls.Add(this.labelTiempo);
            this.Controls.Add(this.labelNMov);
            this.Controls.Add(this.labelMov);
            this.Controls.Add(this.panelTablero);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "OnlineBoard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OnlineBoard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnlineBoard_FormClosing);
            this.Load += new System.EventHandler(this.OnlineBoard_Load);
            this.panelTablero.ResumeLayout(false);
            this._panelP1.ResumeLayout(false);
            this.panelP1.ResumeLayout(false);
            this.panelP1.PerformLayout();
            this._panelPuntuacion.ResumeLayout(false);
            this.panelPuntuacion.ResumeLayout(false);
            this.panelPuntuacion.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelTablero;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel _panelP1;
        private System.Windows.Forms.Panel panelP1;
        private System.Windows.Forms.Button buttonPuntuacionP1;
        private System.Windows.Forms.TextBox textBoxNombreP1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelP1;
        private System.Windows.Forms.Panel _panelPuntuacion;
        private System.Windows.Forms.Panel panelPuntuacion;
        private System.Windows.Forms.Label labelPuntos;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelMoves;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnNewGame;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelNombre;
        private System.Windows.Forms.Label labelMov;
        private System.Windows.Forms.Label labelNMov;
        private System.Windows.Forms.Label labelTiempo;
        private System.Windows.Forms.Label _labelTiempo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textoMensaje;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button botonConectar;
        private System.Windows.Forms.RichTextBox textoChat;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sERVIDORToolStripMenuItem;
    }
}