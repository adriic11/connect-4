﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AplicacionChat;

namespace Conecta_4.Servidor
{
    public partial class Server : Form
    {
        private struct Client
        {
            public EndPoint endPoint;
            public string nombre;
        }

        private ArrayList listaCliente;

        private Socket serverSocket;

        private byte[] dataStream = new byte[1024];

        private delegate void ActualizarEstadoDelegate(string estado);
        private ActualizarEstadoDelegate actualizarEstadoDelegate = null;

        public Server()
        {
            InitializeComponent();
        }

        private void Server_Load(object sender, EventArgs e)
        {
            try
            {
                // Inicializar ArrayList
                this.listaCliente = new ArrayList();

                // Iniciar el actualizador de estado
                this.actualizarEstadoDelegate = new ActualizarEstadoDelegate(this.ActualizarEstado);

                // Inicializa los Sockets
                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                // Inicializa el IPEndPoint en el puerto 30000 para el servidor y el listener.
                IPEndPoint server = new IPEndPoint(IPAddress.Any, 30000);

                //Asocia el socket con el Ip Address y el puerto
                serverSocket.Bind(server);

                // Inicializa el IPEndPoint para los clientes
                IPEndPoint clientes = new IPEndPoint(IPAddress.Any, 0);

                // Inicializa el EndPoint para los clientes
                EndPoint epSender = (EndPoint)clientes;

                // Inicia el listener
                serverSocket.BeginReceiveFrom(this.dataStream, 0, this.dataStream.Length, SocketFlags.None, ref epSender, new AsyncCallback(RecibirDatos), epSender);

                lblStatus.Text = "Listening";
            }
            catch ( Exception ex)
            {
                lblStatus.Text = "Error";
                MessageBox.Show("Error de carga: " + ex.Message, "Servidor UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void EnviarDatos(IAsyncResult asyncResult)
        {
            try
            {
                serverSocket.EndSend(asyncResult);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error al enviar los datos: " + ex.Message, "Servidor UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RecibirDatos(IAsyncResult asyncResult)
        {
            try
            {
                byte[] dato;

                // Inicializamos un objeto paquete para almacenar los datos recibidos
                Paquete datosRecibidos = new Paquete(this.dataStream);

                // Inicializamos un objeto paquete para almacenar los datos enviados
                Paquete enviarDato = new Paquete();

                // Inicializamos el IPEndPoint para los clientes
                IPEndPoint clientes = new IPEndPoint(IPAddress.Any, 0);

                // Inicializamos el EndPoint para los clientes
                EndPoint epSender = (EndPoint)clientes;

                // Recibimos todos los datos
                serverSocket.EndReceiveFrom(asyncResult, ref epSender);

                // Comenzamos el paquete que se va a enviar
                enviarDato.ChatDataID = datosRecibidos.ChatDataID;
                enviarDato.NombreChat = datosRecibidos.NombreChat;

                switch (datosRecibidos.ChatDataID)
                {
                    case DataID.Mensaje:
                        enviarDato.MensajeChat = string.Format("{0}: {1}", datosRecibidos.NombreChat, datosRecibidos.MensajeChat);
                        break;

                    case DataID.LogIn:
                        Client cliente = new Client();
                        cliente.endPoint = epSender;
                        cliente.nombre = datosRecibidos.NombreChat;

                        // Añadir el cliente a la lista
                        this.listaCliente.Add(cliente);

                        enviarDato.MensajeChat = string.Format("-- {0} está online --", datosRecibidos.NombreChat);
                        break;

                    case DataID.LogOut:
                        // Elimina el cliente de la lista
                        foreach (Client c in this.listaCliente)
                        {
                            if (c.endPoint.Equals(epSender))
                            {
                                this.listaCliente.Remove(c);
                                break;
                            }
                        }

                        enviarDato.MensajeChat = string.Format("-- {0} se ha desconectado --", datosRecibidos.NombreChat);
                        break;

                }

                dato = enviarDato.GetDataStream();

                foreach (Client cliente in this.listaCliente)
                {
                    if (cliente.endPoint != epSender || enviarDato.ChatDataID != DataID.LogIn)
                    {
                        serverSocket.BeginSendTo(dato, 0, dato.Length, SocketFlags.None, cliente.endPoint, new AsyncCallback(this.EnviarDatos), cliente.endPoint);
                    }
                }

                serverSocket.BeginReceiveFrom(this.dataStream, 0, this.dataStream.Length, SocketFlags.None, ref epSender, new AsyncCallback(this.RecibirDatos), epSender);

                this.Invoke(this.actualizarEstadoDelegate, new object[] { enviarDato.MensajeChat });
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al recibir datos: " + ex.Message, "Sevidor UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ActualizarEstado(string estado)
        {
            textoEstado.Text += estado + Environment.NewLine;
        }
    }
}
