﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionChat
{ 
    public enum DataID
    {
        Mensaje,
        LogIn,
        LogOut,
        Null
    }

    public class Paquete
    {
        private DataID dataID;
        private string nombre;
        private string mensaje;

        public DataID ChatDataID
        {
            get { return dataID; }
            set { dataID = value; }
        }

        public string NombreChat
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string MensajeChat
        {
            get { return mensaje; }
            set { mensaje = value; }
        }

        public Paquete()
        {
            this.dataID = DataID.Null;
            this.mensaje = null;
            this.nombre = null;
        }

        public Paquete(byte[] dataStream)
        {
            // Lee el identificador del dato desde el principio (4 bytes)
            this.dataID = (DataID)BitConverter.ToInt32(dataStream, 0);

            // Lee la longitud del nombre (4 bytes)
            int longitudNombre = BitConverter.ToInt32(dataStream, 4);

            // Lee la longitud del mensaje (4 bytes)
            int longitudMensaje = BitConverter.ToInt32(dataStream, 8);

            // Lee el campo del nombre
            if (longitudNombre > 0)
                this.nombre = Encoding.UTF8.GetString(dataStream, 12, longitudNombre);
            else
                this.nombre = null;

            // Lee el campo del mensaje
            if (longitudMensaje > 0)
                this.mensaje = Encoding.UTF8.GetString(dataStream, 12 + longitudNombre, longitudMensaje);
            else
                this.mensaje = null;
        }

        // Convierte el paquete en una matriz de bytes para enviar/recibir
        public byte[] GetDataStream()
        {
            List<byte> dataStream = new List<byte>();

            // Añade el identificador del dato
            dataStream.AddRange(BitConverter.GetBytes((int)this.dataID));

            // Añade la longitud del nombre
            if (this.nombre != null)
                dataStream.AddRange(BitConverter.GetBytes(this.nombre.Length));
            else
                dataStream.AddRange(BitConverter.GetBytes(0));

            // Añade la longitud del mensaje
            if (this.mensaje != null)
                dataStream.AddRange(BitConverter.GetBytes(this.mensaje.Length));
            else
                dataStream.AddRange(BitConverter.GetBytes(0));

            // Añade el nombre
            if (this.nombre != null)
                dataStream.AddRange(Encoding.UTF8.GetBytes(this.nombre));

            // Añade el mensaje
            if (this.mensaje != null)
                dataStream.AddRange(Encoding.UTF8.GetBytes(this.mensaje));

            return dataStream.ToArray();
        }

    }
}
