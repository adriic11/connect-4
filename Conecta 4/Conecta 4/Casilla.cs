﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Conecta_4
{
    class Casilla : PictureBox
    {
        private int x, y;
        public bool roja = true;
        public bool vacia = true;
        public bool amarilla = true;
        public int jugador;

        public Casilla(Control padre)
        {
            this.Parent = padre;
        }
        public void posicion(int top, int left)
        {
            this.Top = top;
            this.Left = left;
        }
        public void dimension(int x, int y)
        {
            this.Height = x;
            this.Width = y;
        }
        public int getFila()
        {
            return this.x;
        }
        public int getCol()
        {
            return this.y;
        }
        public void setFila(int px)
        {
            this.x = px;
        }
        public void setCol(int py)
        {
            this.y = py;
        }     
    }
}
