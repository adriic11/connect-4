﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Conecta_4.BBDD
{
    class ScoreDAL
    {
        public static int Agregar(Score pScore)
        {
            int retorno = 0;
            using (SqlConnection conn = BDConexion.ObtenerConexion())
            {
                SqlCommand comando = new SqlCommand(string.Format("Insert into Score (Nombre_jugador, Movimiento_ficha, Tiempo_invertido, Puntuacion, Fecha) values('{0}', '{1}', '{2}', '{3}', '{4}')",
                    pScore.Nombre_jugador, pScore.Movimiento_ficha, pScore.Tiempo_invertido, pScore.Puntuacion, pScore.Fecha), conn);

                retorno = comando.ExecuteNonQuery();
            }
            return retorno;
        }
    }
}
