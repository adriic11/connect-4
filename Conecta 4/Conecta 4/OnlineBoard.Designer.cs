﻿namespace Conecta_4
{
    partial class OnlineBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTablero = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // panelTablero
            // 
            this.panelTablero.Cursor = System.Windows.Forms.Cursors.Default;
            this.panelTablero.Location = new System.Drawing.Point(96, 68);
            this.panelTablero.Name = "panelTablero";
            this.panelTablero.Size = new System.Drawing.Size(692, 493);
            this.panelTablero.TabIndex = 0;
            // 
            // OnlineBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 622);
            this.Controls.Add(this.panelTablero);
            this.Name = "OnlineBoard";
            this.Text = "OnlineBoard";
            this.Load += new System.EventHandler(this.OnlineBoard_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTablero;
    }
}