﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClienteChat;

namespace Conecta_4.Client
{
    public partial class Clientes : Form
    {
        private Socket clienteSocket;

        private string nombre;

        private EndPoint epServer;

        private byte[] dataStream = new byte[1024];

        private delegate void DisplayMessageDelegate(string mensaje);
        private DisplayMessageDelegate displayMessageDelegate = null;

        public Clientes()
        {
            InitializeComponent();
        }


        private void Clientes_Load(object sender, EventArgs e)
        {
            this.displayMessageDelegate = new DisplayMessageDelegate(this.DisplayMensaje);
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                Paquete enviarDato = new Paquete();
                enviarDato.NombreChat = this.nombre;
                enviarDato.MensajeChat = txtMensaje.Text.Trim();
                enviarDato.ChatDataID = DataID.Mensaje;

                byte[] byteDato = enviarDato.GetDataStream();

                clienteSocket.BeginSendTo(byteDato, 0, byteDato.Length, SocketFlags.None, epServer, new AsyncCallback(this.EnviarDato), null);

                txtMensaje.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al enviar: " + ex.Message, "Cliente UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Clientes_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (this.clienteSocket != null)
                {
                    Paquete enviarDato = new Paquete();
                    enviarDato.ChatDataID = DataID.LogOut;
                    enviarDato.NombreChat = this.nombre;
                    enviarDato.MensajeChat = null;

                    byte[] byteDato = enviarDato.GetDataStream();

                    this.clienteSocket.SendTo(byteDato, 0, byteDato.Length, SocketFlags.None, epServer);

                    this.clienteSocket.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al cerrar: " + ex.Message, "Cliente UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void botonConectar_Click(object sender, EventArgs e)
        {
            try
            {
                this.nombre = txtNombre.Text.Trim();

                Paquete enviarDato = new Paquete();
                enviarDato.NombreChat = this.nombre;
                enviarDato.MensajeChat = null;
                enviarDato.ChatDataID = DataID.LogIn;

                this.clienteSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Tcp);

                IPAddress serverIP = IPAddress.Parse(txtServerIP.Text.Trim());

                IPEndPoint server = new IPEndPoint(serverIP, 30000);

                epServer = (EndPoint)server;

                byte[] dato = enviarDato.GetDataStream();

                clienteSocket.BeginSendTo(dato, 0, dato.Length, SocketFlags.None, epServer, new AsyncCallback(this.EnviarDato), null);

                this.dataStream = new byte[1024];

                clienteSocket.BeginReceiveFrom(this.dataStream, 0, this.dataStream.Length, SocketFlags.None, ref epServer, new AsyncCallback(this.RecibirDato), null);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de conexión: " + ex.Message, "Cliente UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void EnviarDato(IAsyncResult ar)
        {
            try
            {
                clienteSocket.EndSend(ar);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Dato enviado: " + ex.Message, "Cliente UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RecibirDato(IAsyncResult ar)
        {
            try
            {
                this.clienteSocket.EndReceive(ar);

                Paquete recibirDato = new Paquete(this.dataStream);

                if (recibirDato.MensajeChat != null)
                    this.Invoke(this.displayMessageDelegate, new object[] { recibirDato.MensajeChat });

                this.dataStream = new byte[1024];

                clienteSocket.BeginReceiveFrom(this.dataStream, 0, this.dataStream.Length, SocketFlags.None, ref epServer, new AsyncCallback(this.RecibirDato), null);
            }
            catch (ObjectDisposedException)
            { }
            catch (Exception ex)
            {
                MessageBox.Show("Dato recibido: " + ex.Message, "Cliente UDP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DisplayMensaje(string mensaje)
        {
            txtChat.Text += mensaje + Environment.NewLine;
        }

       
    }
}