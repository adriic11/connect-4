﻿namespace Conecta_4
{
    partial class Board
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.configuraciónToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editarTableroToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaPartidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reiniciarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reiniciarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirChatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirChatToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ocultarChatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarTableroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conexionBBDD = new System.Windows.Forms.ToolStripMenuItem();
            this.labelConfig = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxFila = new System.Windows.Forms.TextBox();
            this.textBoxCol = new System.Windows.Forms.TextBox();
            this.botonAplicar = new System.Windows.Forms.Button();
            this.botonCancelar = new System.Windows.Forms.Button();
            this.panelConfig = new System.Windows.Forms.Panel();
            this.panelTablero = new System.Windows.Forms.Panel();
            this._panelPuntuacion = new System.Windows.Forms.Panel();
            this.panelPuntuacion = new System.Windows.Forms.Panel();
            this.labelPuntos = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelMoves = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.buttonHome = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelNombre = new System.Windows.Forms.Label();
            this._panelP1 = new System.Windows.Forms.Panel();
            this.panelP1 = new System.Windows.Forms.Panel();
            this.buttonPuntuacionP1 = new System.Windows.Forms.Button();
            this.textBoxNombreP1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelP1 = new System.Windows.Forms.Label();
            this._labelTiempo = new System.Windows.Forms.Label();
            this.labelTiempo = new System.Windows.Forms.Label();
            this.labelNMov = new System.Windows.Forms.Label();
            this.labelMov = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.panelConfig.SuspendLayout();
            this.panelTablero.SuspendLayout();
            this._panelPuntuacion.SuspendLayout();
            this.panelPuntuacion.SuspendLayout();
            this._panelP1.SuspendLayout();
            this.panelP1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraciónToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(861, 24);
            this.menuStrip1.TabIndex = 91;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // configuraciónToolStripMenuItem1
            // 
            this.configuraciónToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editarTableroToolStripMenuItem1});
            this.configuraciónToolStripMenuItem1.Name = "configuraciónToolStripMenuItem1";
            this.configuraciónToolStripMenuItem1.Size = new System.Drawing.Size(95, 20);
            this.configuraciónToolStripMenuItem1.Text = "Configuración";
            // 
            // editarTableroToolStripMenuItem1
            // 
            this.editarTableroToolStripMenuItem1.Name = "editarTableroToolStripMenuItem1";
            this.editarTableroToolStripMenuItem1.Size = new System.Drawing.Size(145, 22);
            this.editarTableroToolStripMenuItem1.Text = "Editar Tablero";
            this.editarTableroToolStripMenuItem1.Click += new System.EventHandler(this.editarTableroToolStripMenuItem1_Click);
            // 
            // nuevaPartidaToolStripMenuItem
            // 
            this.nuevaPartidaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevaToolStripMenuItem,
            this.reiniciarToolStripMenuItem1});
            this.nuevaPartidaToolStripMenuItem.Name = "nuevaPartidaToolStripMenuItem";
            this.nuevaPartidaToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.nuevaPartidaToolStripMenuItem.Text = "Partida";
            // 
            // nuevaToolStripMenuItem
            // 
            this.nuevaToolStripMenuItem.Name = "nuevaToolStripMenuItem";
            this.nuevaToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.nuevaToolStripMenuItem.Text = "Nueva";
            this.nuevaToolStripMenuItem.Click += new System.EventHandler(this.nuevaToolStripMenuItem_Click);
            // 
            // reiniciarToolStripMenuItem1
            // 
            this.reiniciarToolStripMenuItem1.Name = "reiniciarToolStripMenuItem1";
            this.reiniciarToolStripMenuItem1.Size = new System.Drawing.Size(119, 22);
            this.reiniciarToolStripMenuItem1.Text = "Reiniciar";
            // 
            // reiniciarToolStripMenuItem
            // 
            this.reiniciarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirChatToolStripMenuItem,
            this.abrirChatToolStripMenuItem1,
            this.ocultarChatToolStripMenuItem});
            this.reiniciarToolStripMenuItem.Name = "reiniciarToolStripMenuItem";
            this.reiniciarToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.reiniciarToolStripMenuItem.Text = "Chat";
            // 
            // abrirChatToolStripMenuItem
            // 
            this.abrirChatToolStripMenuItem.Name = "abrirChatToolStripMenuItem";
            this.abrirChatToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.abrirChatToolStripMenuItem.Text = "Conectar Servidor";
            this.abrirChatToolStripMenuItem.Click += new System.EventHandler(this.abrirChatToolStripMenuItem_Click);
            // 
            // abrirChatToolStripMenuItem1
            // 
            this.abrirChatToolStripMenuItem1.Name = "abrirChatToolStripMenuItem1";
            this.abrirChatToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.abrirChatToolStripMenuItem1.Text = "Abrir Chat";
            this.abrirChatToolStripMenuItem1.Click += new System.EventHandler(this.abrirChatToolStripMenuItem1_Click);
            // 
            // ocultarChatToolStripMenuItem
            // 
            this.ocultarChatToolStripMenuItem.Name = "ocultarChatToolStripMenuItem";
            this.ocultarChatToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.ocultarChatToolStripMenuItem.Text = "Ocultar Chat";
            // 
            // configuraciónToolStripMenuItem
            // 
            this.configuraciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editarTableroToolStripMenuItem,
            this.conexionBBDD});
            this.configuraciónToolStripMenuItem.Name = "configuraciónToolStripMenuItem";
            this.configuraciónToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.configuraciónToolStripMenuItem.Text = "Configuración";
            // 
            // editarTableroToolStripMenuItem
            // 
            this.editarTableroToolStripMenuItem.Name = "editarTableroToolStripMenuItem";
            this.editarTableroToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.editarTableroToolStripMenuItem.Text = "Editar tablero";
            // 
            // conexionBBDD
            // 
            this.conexionBBDD.Name = "conexionBBDD";
            this.conexionBBDD.Size = new System.Drawing.Size(158, 22);
            this.conexionBBDD.Text = "Conexion BBDD";
            // 
            // labelConfig
            // 
            this.labelConfig.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelConfig.Location = new System.Drawing.Point(1, 1);
            this.labelConfig.Name = "labelConfig";
            this.labelConfig.Size = new System.Drawing.Size(199, 33);
            this.labelConfig.TabIndex = 0;
            this.labelConfig.Text = "Seleccione la configuración deseada";
            this.labelConfig.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Filas: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Columnas:";
            // 
            // textBoxFila
            // 
            this.textBoxFila.Location = new System.Drawing.Point(93, 55);
            this.textBoxFila.Name = "textBoxFila";
            this.textBoxFila.Size = new System.Drawing.Size(63, 20);
            this.textBoxFila.TabIndex = 3;
            // 
            // textBoxCol
            // 
            this.textBoxCol.Location = new System.Drawing.Point(93, 97);
            this.textBoxCol.Name = "textBoxCol";
            this.textBoxCol.Size = new System.Drawing.Size(63, 20);
            this.textBoxCol.TabIndex = 4;
            // 
            // botonAplicar
            // 
            this.botonAplicar.Location = new System.Drawing.Point(16, 179);
            this.botonAplicar.Name = "botonAplicar";
            this.botonAplicar.Size = new System.Drawing.Size(75, 23);
            this.botonAplicar.TabIndex = 5;
            this.botonAplicar.Text = "Aplicar";
            this.botonAplicar.UseVisualStyleBackColor = true;
            this.botonAplicar.Click += new System.EventHandler(this.botonAplicar_Click);
            // 
            // botonCancelar
            // 
            this.botonCancelar.Location = new System.Drawing.Point(110, 179);
            this.botonCancelar.Name = "botonCancelar";
            this.botonCancelar.Size = new System.Drawing.Size(75, 23);
            this.botonCancelar.TabIndex = 6;
            this.botonCancelar.Text = "Cancelar";
            this.botonCancelar.UseVisualStyleBackColor = true;
            // 
            // panelConfig
            // 
            this.panelConfig.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelConfig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelConfig.Controls.Add(this.labelConfig);
            this.panelConfig.Controls.Add(this.botonCancelar);
            this.panelConfig.Controls.Add(this.botonAplicar);
            this.panelConfig.Controls.Add(this.textBoxCol);
            this.panelConfig.Controls.Add(this.textBoxFila);
            this.panelConfig.Controls.Add(this.label3);
            this.panelConfig.Controls.Add(this.label2);
            this.panelConfig.Location = new System.Drawing.Point(12, 81);
            this.panelConfig.Name = "panelConfig";
            this.panelConfig.Size = new System.Drawing.Size(201, 212);
            this.panelConfig.TabIndex = 92;
            this.panelConfig.Visible = false;
            // 
            // panelTablero
            // 
            this.panelTablero.Controls.Add(this._panelPuntuacion);
            this.panelTablero.Location = new System.Drawing.Point(147, 81);
            this.panelTablero.Name = "panelTablero";
            this.panelTablero.Size = new System.Drawing.Size(572, 486);
            this.panelTablero.TabIndex = 93;
            // 
            // _panelPuntuacion
            // 
            this._panelPuntuacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this._panelPuntuacion.Controls.Add(this.panelPuntuacion);
            this._panelPuntuacion.Location = new System.Drawing.Point(336, 70);
            this._panelPuntuacion.Name = "_panelPuntuacion";
            this._panelPuntuacion.Size = new System.Drawing.Size(207, 327);
            this._panelPuntuacion.TabIndex = 94;
            // 
            // panelPuntuacion
            // 
            this.panelPuntuacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelPuntuacion.Controls.Add(this.labelPuntos);
            this.panelPuntuacion.Controls.Add(this.label10);
            this.panelPuntuacion.Controls.Add(this.labelMoves);
            this.panelPuntuacion.Controls.Add(this.label8);
            this.panelPuntuacion.Controls.Add(this.button3);
            this.panelPuntuacion.Controls.Add(this.buttonHome);
            this.panelPuntuacion.Controls.Add(this.button1);
            this.panelPuntuacion.Controls.Add(this.label7);
            this.panelPuntuacion.Controls.Add(this.label6);
            this.panelPuntuacion.Controls.Add(this.labelNombre);
            this.panelPuntuacion.Location = new System.Drawing.Point(3, 3);
            this.panelPuntuacion.Name = "panelPuntuacion";
            this.panelPuntuacion.Size = new System.Drawing.Size(200, 320);
            this.panelPuntuacion.TabIndex = 93;
            // 
            // labelPuntos
            // 
            this.labelPuntos.AutoSize = true;
            this.labelPuntos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPuntos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.labelPuntos.Location = new System.Drawing.Point(78, 154);
            this.labelPuntos.Name = "labelPuntos";
            this.labelPuntos.Size = new System.Drawing.Size(46, 13);
            this.labelPuntos.TabIndex = 13;
            this.labelPuntos.Text = "Puntos";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(140, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Moves";
            // 
            // labelMoves
            // 
            this.labelMoves.AutoSize = true;
            this.labelMoves.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMoves.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.labelMoves.Location = new System.Drawing.Point(30, 86);
            this.labelMoves.Name = "labelMoves";
            this.labelMoves.Size = new System.Drawing.Size(44, 13);
            this.labelMoves.TabIndex = 11;
            this.labelMoves.Text = "Moves";
            this.labelMoves.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(62, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 16);
            this.label8.TabIndex = 10;
            this.label8.Text = "Puntuación";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button3.Location = new System.Drawing.Point(70, 231);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "New Game";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // buttonHome
            // 
            this.buttonHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.buttonHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHome.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonHome.Location = new System.Drawing.Point(70, 266);
            this.buttonHome.Name = "buttonHome";
            this.buttonHome.Size = new System.Drawing.Size(75, 23);
            this.buttonHome.TabIndex = 8;
            this.buttonHome.Text = "Home";
            this.buttonHome.UseVisualStyleBackColor = false;
            this.buttonHome.Click += new System.EventHandler(this.buttonHome_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(68, 195);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(123, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Tiempo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(22, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Moves";
            // 
            // labelNombre
            // 
            this.labelNombre.AutoSize = true;
            this.labelNombre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNombre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.labelNombre.Location = new System.Drawing.Point(62, 22);
            this.labelNombre.Name = "labelNombre";
            this.labelNombre.Size = new System.Drawing.Size(81, 18);
            this.labelNombre.TabIndex = 4;
            this.labelNombre.Text = "NOMBRE";
            this.labelNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _panelP1
            // 
            this._panelP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this._panelP1.Controls.Add(this.panelP1);
            this._panelP1.Location = new System.Drawing.Point(672, 43);
            this._panelP1.Name = "_panelP1";
            this._panelP1.Size = new System.Drawing.Size(177, 205);
            this._panelP1.TabIndex = 97;
            // 
            // panelP1
            // 
            this.panelP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelP1.Controls.Add(this.buttonPuntuacionP1);
            this.panelP1.Controls.Add(this.textBoxNombreP1);
            this.panelP1.Controls.Add(this.label1);
            this.panelP1.Controls.Add(this.labelP1);
            this.panelP1.Location = new System.Drawing.Point(3, 3);
            this.panelP1.Name = "panelP1";
            this.panelP1.Size = new System.Drawing.Size(171, 199);
            this.panelP1.TabIndex = 94;
            // 
            // buttonPuntuacionP1
            // 
            this.buttonPuntuacionP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.buttonPuntuacionP1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPuntuacionP1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.buttonPuntuacionP1.Location = new System.Drawing.Point(50, 143);
            this.buttonPuntuacionP1.Name = "buttonPuntuacionP1";
            this.buttonPuntuacionP1.Size = new System.Drawing.Size(75, 23);
            this.buttonPuntuacionP1.TabIndex = 3;
            this.buttonPuntuacionP1.Text = "Puntuación";
            this.buttonPuntuacionP1.UseVisualStyleBackColor = false;
            this.buttonPuntuacionP1.Click += new System.EventHandler(this.buttonPuntuacionP1_Click_1);
            // 
            // textBoxNombreP1
            // 
            this.textBoxNombreP1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxNombreP1.Location = new System.Drawing.Point(37, 96);
            this.textBoxNombreP1.Name = "textBoxNombreP1";
            this.textBoxNombreP1.Size = new System.Drawing.Size(95, 20);
            this.textBoxNombreP1.TabIndex = 2;
            this.textBoxNombreP1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(28, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Introduzca su nombre :";
            // 
            // labelP1
            // 
            this.labelP1.AutoSize = true;
            this.labelP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.labelP1.Location = new System.Drawing.Point(19, 15);
            this.labelP1.Name = "labelP1";
            this.labelP1.Size = new System.Drawing.Size(137, 18);
            this.labelP1.TabIndex = 0;
            this.labelP1.Text = "PLAYER 1 WINS!";
            // 
            // _labelTiempo
            // 
            this._labelTiempo.AutoSize = true;
            this._labelTiempo.Font = new System.Drawing.Font("Mongolian Baiti", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._labelTiempo.Location = new System.Drawing.Point(378, 43);
            this._labelTiempo.Name = "_labelTiempo";
            this._labelTiempo.Size = new System.Drawing.Size(89, 23);
            this._labelTiempo.TabIndex = 278;
            this._labelTiempo.Text = "Tiempo:";
            // 
            // labelTiempo
            // 
            this.labelTiempo.AutoSize = true;
            this.labelTiempo.Font = new System.Drawing.Font("Yu Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTiempo.Location = new System.Drawing.Point(464, 37);
            this.labelTiempo.Name = "labelTiempo";
            this.labelTiempo.Size = new System.Drawing.Size(83, 35);
            this.labelTiempo.TabIndex = 277;
            this.labelTiempo.Text = "00:00";
            // 
            // labelNMov
            // 
            this.labelNMov.AutoSize = true;
            this.labelNMov.Font = new System.Drawing.Font("Yu Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNMov.Location = new System.Drawing.Point(240, 39);
            this.labelNMov.Name = "labelNMov";
            this.labelNMov.Size = new System.Drawing.Size(28, 31);
            this.labelNMov.TabIndex = 276;
            this.labelNMov.Text = "0";
            // 
            // labelMov
            // 
            this.labelMov.AutoSize = true;
            this.labelMov.Font = new System.Drawing.Font("Mongolian Baiti", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMov.Location = new System.Drawing.Point(101, 43);
            this.labelMov.Name = "labelMov";
            this.labelMov.Size = new System.Drawing.Size(142, 23);
            this.labelMov.TabIndex = 275;
            this.labelMov.Text = "Movimientos:";
            // 
            // Board
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Moccasin;
            this.ClientSize = new System.Drawing.Size(861, 579);
            this.Controls.Add(this.panelConfig);
            this.Controls.Add(this._panelP1);
            this.Controls.Add(this._labelTiempo);
            this.Controls.Add(this.labelTiempo);
            this.Controls.Add(this.labelNMov);
            this.Controls.Add(this.labelMov);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panelTablero);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Board";
            this.Text = "Board";
            this.Load += new System.EventHandler(this.Board_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelConfig.ResumeLayout(false);
            this.panelConfig.PerformLayout();
            this.panelTablero.ResumeLayout(false);
            this._panelPuntuacion.ResumeLayout(false);
            this.panelPuntuacion.ResumeLayout(false);
            this.panelPuntuacion.PerformLayout();
            this._panelP1.ResumeLayout(false);
            this.panelP1.ResumeLayout(false);
            this.panelP1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nuevaPartidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reiniciarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reiniciarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirChatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ocultarChatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarTableroToolStripMenuItem;
        private System.Windows.Forms.Label labelConfig;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxFila;
        private System.Windows.Forms.TextBox textBoxCol;
        private System.Windows.Forms.Button botonAplicar;
        private System.Windows.Forms.Button botonCancelar;
        private System.Windows.Forms.Panel panelConfig;
        private System.Windows.Forms.Panel panelTablero;
        private System.Windows.Forms.ToolStripMenuItem conexionBBDD;
        private System.Windows.Forms.Panel _panelPuntuacion;
        private System.Windows.Forms.Panel panelPuntuacion;
        private System.Windows.Forms.Label labelNombre;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button buttonHome;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelPuntos;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelMoves;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel _panelP1;
        private System.Windows.Forms.Panel panelP1;
        private System.Windows.Forms.Button buttonPuntuacionP1;
        private System.Windows.Forms.TextBox textBoxNombreP1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelP1;
        private System.Windows.Forms.ToolStripMenuItem abrirChatToolStripMenuItem1;
        private System.Windows.Forms.Label _labelTiempo;
        private System.Windows.Forms.Label labelTiempo;
        private System.Windows.Forms.Label labelNMov;
        private System.Windows.Forms.Label labelMov;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editarTableroToolStripMenuItem1;
    }
}