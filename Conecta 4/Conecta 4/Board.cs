﻿using Conecta_4.BBDD;
using Conecta_4.Servidor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Conecta_4.Client;

namespace Conecta_4
{
   
    public partial class Board : Form
    {
        Casilla[,] tablero;
        public int fila, col;
        public int jugador = 0;     //Controla el turno del jugador
        public int red = 0;
        public int yellow = 0;
        public int movimientos = 0;
        private int min, sec;
        System.Timers.Timer t;
        Connect_4Entities con = new Connect_4Entities();
        Score sc;
        private int puntuacion = 0;

        public Board(int fila, int col)
        {
            InitializeComponent();
            this.fila = fila;
            this.col = col;
        }

        public int getNumFilas()
        {
            return fila;
        }
        
        public int getNumColumnas()
        {
            return col;
        }

        private void casillaClick(object sender, EventArgs e)
        {
            int columna;
            int fila;
            int nCol, nFila;

            nCol = getNumColumnas();
            nFila = getNumFilas();

            columna=((Casilla)sender).getCol();
            fila = ((Casilla)sender).getFila();
            if (fila <= getNumFilas())
            {
                pintarFicha(columna,1);
                if (ganar(1)==1)
                {
                    _panelP1.Visible = true;
                    labelP1.Text = "PLAYER 2 WINS!";
                    _panelP1.BringToFront();
                }
                MovFicha();
            }
        }

        // Transforma los segundos en minutos y lo imprime 
        private void OnTimeEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            Invoke(new Action(() =>
            {
                sec += 1;
                if (sec == 60)
                {
                    sec = 0;
                    min += 1;
                }
                labelTiempo.Text = string.Format("{0}:{1}", min.ToString().PadLeft(2, '0'), sec.ToString().PadLeft(2, '0'));
            }));
        }

        private void MovFicha()
        {
            movimientos++;
            labelNMov.Text = movimientos.ToString();
        }

        private void CalcularPuntuacion()
        {
            int tiempo;
            int mov = 0;
            sec *= 5;
            min *= 10;
            movimientos = mov;
            mov *= 100;
            tiempo = sec + min;
            mov -= tiempo;
            puntuacion = mov;
        }

        private void pintarFicha (int col, int jugador)
        {          
            if (jugador == 1)
            {
                for (int i = 0; i < getNumFilas(); i++)
                {
                    if (tablero[i, col].vacia)
                    {
                        if (((i - 1) < getNumFilas() - 1) && (i > 0))
                        {
                            tablero[i-1, col].Image = Properties.Resources.Vacia;
                            tablero[i-1, col].SizeMode = PictureBoxSizeMode.StretchImage;
                            
                        }

                        tablero[i, col].Image = Properties.Resources.fichaRoja;
                        tablero[i, col].SizeMode = PictureBoxSizeMode.StretchImage;
                        tablero[i, col].jugador = 1;
                        tablero[i, col].roja = true;

                        if (i == (getNumFilas() - 1))
                        {
                            tablero[i, col].Image = Properties.Resources.fichaRoja;
                            tablero[i, col].SizeMode = PictureBoxSizeMode.StretchImage;
                            tablero[i, col].roja = true;
                            tablero[i, col].jugador = 1;
                            tablero[i, col].vacia = false;
                            //if (tablero[i, col].jugador == 1 && ganar(i, col, 1) == 1)
                            //{
                            //    _panelP1.Visible = true;
                            //    labelP1.Text = "PLAYER 1 WINS!";
                            //    _panelP1.BringToFront();
                            //}
                        }
                    }                   
                    else if (i > 0)
                    {
                        tablero[i - 1, col].vacia = false;
                        tablero[i - 1, col].Image = Properties.Resources.fichaRoja;
                        tablero[i - 1, col].SizeMode = PictureBoxSizeMode.StretchImage;
                        tablero[i - 1, col].roja = true;
                        tablero[i - 1, col].jugador = 1;
                        //if (tablero[i - 1, col].jugador == 1 && ganar(i - 1, col, 1) == 1)
                        //{
                        //    _panelP1.Visible = true;
                        //    labelP1.Text = "PLAYER 1 WINS!";
                        //    _panelP1.BringToFront();
                        //}
                        break;
                    }                   
                    Task.Delay(3000);
                    this.Refresh();                    
                }                            
                jugador = 2;
            }
            else
            {
                for (int i = 0; i < getNumFilas(); i++)
                {
                    if (tablero[i, col].vacia)
                    {
                        if (((i - 1) < getNumFilas() - 1) && (i > 0))
                        {
                            tablero[i - 1, col].Image = Properties.Resources.Vacia;
                            tablero[i - 1, col].SizeMode = PictureBoxSizeMode.StretchImage;                           
                        }

                        tablero[i, col].Image = Properties.Resources.fichaAmarilla;
                        tablero[i, col].SizeMode = PictureBoxSizeMode.StretchImage;
                        tablero[i, col].jugador = 2;
                        tablero[i, col].amarilla = true;

                        if (i == (getNumFilas() - 1))
                        {
                            tablero[i, col].vacia = false;
                            tablero[i, col].jugador = 2;
                            //if (tablero[i,col].jugador == 2 && ganar(i, col, 2) == 1)
                            //{
                            //    _panelP1.Visible = true;
                            //    labelP1.Text = "PLAYER 2 WINS!";
                            //    _panelP1.BringToFront();
                            //}                               
                        }
                    }                   
                    else if (i > 0)
                    {
                        tablero[i - 1, col].vacia = false;
                        tablero[i-1, col].jugador = 2;
                        //if (tablero[i-1,col].jugador == 2 && ganar(i - 1, col, 2) == 1)
                        //{
                        //    _panelP1.Visible = true;
                        //    labelP1.Text = "PLAYER 2 WINS!";
                        //    _panelP1.BringToFront();
                        //}                           
                        break;
                    }                  
                    Task.Delay(3000);
                    this.Refresh();
                }         
                jugador = 1;
            }                 
        }


        public int ganar(int jugador)
        {
            int contador_ganar = 4;
            int contador = 0;


            //horizontal -
            //for (int c = 0; c < getNumColumnas() - 3; c++)
            //{
            //    for (int f = 0; f < getNumFilas(); f++)
            //    {
            //        if (!tablero[f, c].vacia && tablero[f, c].jugador == jugador && jugador == tablero[f, c + 1].jugador && jugador == tablero[f, c + 2].jugador && jugador == tablero[f, c + 3].jugador)
            //        {
            //            t.Stop();
            //            CalcularPuntuacion();
            //            return 1;
            //        }
            //    }


            //    if (contador == contador_ganar)
            //    {
            //        t.Stop();
            //        CalcularPuntuacion();
            //        return 1;
            //    }                              
            //}   

            //vertical |
            //for (int f= fila_actual; f <= getNumFilas()-1 ; f++)
            //{

            //    if (!tablero[f, columna_actual].vacia && jugador == tablero[f, columna_actual].jugador)
            //    {
            //        contador++;
            //    }
            //    else
            //        break;

            //    if (contador == contador_ganar)
            //    {
            //        t.Stop();
            //        CalcularPuntuacion();
            //        return 1;
            //    }

            //}

            //diagonal descendente \  

            //for (int f = fila_actual; f <= getNumFilas() - 1; f++)
            //{
            //    for (int c = columna_actual; c <= getNumColumnas() - 1; c++)
            //    {
            //        if (jugador == 1)
            //        {
            //            if (!tablero[f, c].vacia)
            //            {
            //                //if(jugador == tablero[f,c].jugador && tablero[f,c].jugador == tablero[f+1,c-1].jugador && tablero[f,c].jugador == tablero[f+2,c-2].jugador && tablero[f,c].jugador == tablero[f + 3, c - 3].jugador)
            //                if (f <= getNumFilas() - 4 && c >= getNumColumnas() - 4 && jugador == tablero[f, c].jugador && jugador == tablero[f + 1, c - 1].jugador && jugador == tablero[f + 2, c - 2].jugador && jugador == tablero[f + 3, c - 3].jugador)
            //                    contador = 4;

            //                if (f >= getNumFilas() && jugador == tablero[f, c].jugador && jugador == tablero[f - 1, c + 1].jugador && jugador == tablero[f - 2, c + 2].jugador && jugador == tablero[f - 3, c + 3].jugador)
            //                    contador = 4;
            //            }
            //        }
            //        if (jugador == 2)
            //        {
            //            if (!tablero[f, c].vacia)
            //            {
            //                //if(jugador == tablero[f,c].jugador && tablero[f,c].jugador == tablero[f+1,c-1].jugador && tablero[f,c].jugador == tablero[f+2,c-2].jugador && tablero[f,c].jugador == tablero[f + 3, c - 3].jugador)
            //                if (f <= getNumFilas() - 4 && c >= getNumColumnas() - 4 && jugador == tablero[f, c].jugador && jugador == tablero[f + 1, c - 1].jugador && jugador == tablero[f + 2, c - 2].jugador && jugador == tablero[f + 3, c - 3].jugador)
            //                    contador = 4;

            //                if (f >= getNumFilas() && jugador == tablero[f, c].jugador && jugador == tablero[f - 1, c + 1].jugador && jugador == tablero[f - 2, c + 2].jugador && jugador == tablero[f - 3, c + 3].jugador)
            //                    contador = 4;
            //            }
            //        }
            //        if (contador == contador_ganar)
            //        {
            //            t.Stop();
            //            CalcularPuntuacion();
            //            return 1;
            //        }
            //    }
            //}

            //diagonal ascendente /
            for (int f = 4; f < 6; f++)
            {
                for (int c = 1; c<4; c++)
                {
                    if (jugador == 1 && tablero[f, c].jugador == 1 && tablero[f-1, c+1].jugador == 1 && tablero[f-2, c+2].jugador == 1 && tablero[f-3, c+3].jugador == 1)
                    {
                        t.Stop();
                        CalcularPuntuacion();

                        return 1;
                    }
                }
            }

            //for (int f=1; f<3; f++)
            //{
            //    for (int c=1; c<4; c++)
            //    {
            //        if (tablero[f, c].jugador == 1 && tablero[f + 1, c + 1].jugador == 1 && tablero[f + 2, c + 2].jugador == 1 && tablero[f + 3, c + 3].jugador == 1 )
            //        {
            //            t.Stop();
            //            CalcularPuntuacion();
            //            return 1;
            //        }
            //    }
            //}
            //for (int f = fila_actual; f <= getNumFilas() - 1; f++)
            //{
            //    for (int c = columna_actual; c <= getNumColumnas() - 1; c++)
            //    {
            //        if (jugador == 1)
            //        {
            //            if (!tablero[f, c].vacia)
            //            {
            //                if (c >= getNumColumnas() - 3 && f >= getNumFilas() - 3 && jugador == tablero[f, c].jugador && jugador == tablero[f - 1, c - 1].jugador && jugador == tablero[f - 2, c - 2].jugador && jugador == tablero[f - 3, c - 3].jugador)
            //                    contador = 4;
            //                if (c <= getNumColumnas() - 4 && f <= getNumFilas() - 4 && jugador == tablero[f, c].jugador && jugador == tablero[f + 1, c + 1].jugador && jugador == tablero[f + 2, c + 2].jugador && jugador == tablero[f + 3, c + 3].jugador)
            //                    contador = 4;
            //            }
            //        }
            //        if (jugador == 2)
            //        {
            //            if (!tablero[f, c].vacia)
            //            {
            //                if (c >= getNumColumnas() - 3 && f >= getNumFilas() - 3 && jugador == tablero[f, c].jugador && jugador == tablero[f - 1, c - 1].jugador && jugador == tablero[f - 2, c - 2].jugador && jugador == tablero[f - 3, c - 3].jugador)
            //                    contador = 4;
            //                if (c <= getNumColumnas() - 4 && f <= getNumFilas() - 4 && jugador == tablero[f, c].jugador && jugador == tablero[f + 1, c + 1].jugador && jugador == tablero[f + 2, c + 2].jugador && jugador == tablero[f + 3, c + 3].jugador)
            //                    contador = 4;
            //            }
            //        }
            //        if (contador == contador_ganar)
            //        {
            //            t.Stop();
            //            CalcularPuntuacion();
            //            return 1;
            //        }
            //    }
            //}
            return 0;           
        }
        

        public void crearTablero (int fila, int col)
        {
            tablero = new Casilla[fila, col];
            for (int f = 0; f < fila; f++)
                for (int c = 0; c < col; c++)
                {
                    tablero[f, c] = new Casilla(panelTablero);
                    tablero[f, c].setFila (f);
                    tablero[f, c].setCol (c);
                    tablero[f, c].posicion(60 * f, 60 * c);
                    tablero[f, c].dimension(60, 60);
                    tablero[f, c].Image =  Properties.Resources.Vacia;
                    tablero[f, c].SizeMode = PictureBoxSizeMode.StretchImage;
                    tablero[f, c].Visible = true;
                    tablero[f, c].BringToFront();
                    tablero[f, c].Click += casillaClick;
                }
            this.Refresh();
        }

        private void nuevaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ficha = new Casilla(panelTablero);
            ficha.posicion(0, 0);
            ficha.dimension(40,40);
            ficha.ImageLocation = @"C:\Users\acarrasco\Desktop\pictureBox1.Image.png";
            ficha.SizeMode = PictureBoxSizeMode.StretchImage;
            ficha.Visible = true;
            ficha.BringToFront();
            this.Refresh();
        }
       
       
        private void botonAplicar_Click(object sender, EventArgs e)
        {
            int valorFila = int.Parse(textBoxFila.Text);
            int valorCol = int.Parse(textBoxCol.Text);
            col = valorCol;
            fila = valorFila;
            crearTablero(fila, col);
            this.Refresh();
        }


        private void Board_Load(object sender, EventArgs e)
        {
            crearTablero(6, 7);
            _panelP1.Visible = false;
            _panelPuntuacion.Visible = false;
            t = new System.Timers.Timer();
            t.Start();
            t.Interval = 1000; // 1 sec
            t.Elapsed += OnTimeEvent;

        }

        private void buttonPuntuacionP1_Click_1(object sender, EventArgs e)
        {
            _panelPuntuacion.Visible = true;
            _panelPuntuacion.BringToFront();
            panelTablero.Visible = false;
            string nombre = textBoxNombreP1.Text;
            labelNombre.Text = nombre;
            string mov = Convert.ToString(movimientos);
            labelMoves.Text = mov;
            label10.Text = labelTiempo.Text;
            labelPuntos.Text = puntuacion.ToString() + " Pts";

            sc = new Score();
            sc.Nombre_jugador = textBoxNombreP1.Text;
            sc.Movimiento_ficha = movimientos;
            sc.Puntuacion = labelPuntos.Text;
            sc.Tiempo_invertido = TimeSpan.Parse(labelTiempo.Text);

            con.Score.Add(sc);
            con.SaveChanges();
        }

       
        private void buttonHome_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Close();
        }

        private void abrirChatToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Clientes cl = new Clientes();
            cl.Show();
        }

        private void editarTableroToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            panelConfig.BringToFront();
            panelConfig.Visible = true;
        }

        private void abrirChatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Server sv = new Server();
            sv.Show();
        }

    }
}