﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Conecta_4.Multiplayer;

namespace Conecta_4
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Board tablero = new Board(6,7);
            tablero.Show();    
        }

        private void botonOnline_Click(object sender, EventArgs e)
        {
            ServerConect sc = new ServerConect();
            sc.Show();            
        }
    }
}
