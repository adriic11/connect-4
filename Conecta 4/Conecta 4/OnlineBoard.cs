﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Conecta_4
{
    public partial class OnlineBoard : Form
    {
        Casilla[,] tablero;
        public int fila, col;
        public int jugador;
        public int movimientos = 0;
        private TcpListener SocketEscucha;
        private TcpClient SocketEnvio;
        private Socket SocketRecepcion;
        private BackgroundWorker MensajeRecibido = new BackgroundWorker();
        private Bitmap CasillaRecibida;

        public OnlineBoard(int fila, int col)
        {
            InitializeComponent();
            this.fila = fila;
            this.col = col;
            Thread HiloEscuchar = new Thread(new ThreadStart(HILO_EscucharConexiones));
        }

        public int getNumFilas()
        {
            return fila;
        }

        public int getNumColumnas()
        {
            return col;
        }

        private void OnlineBoard_Load(object sender, EventArgs e)
        {
            // Creo el hilo que escuchará las conexiones pendientes
            crearTablero(6, 7);
            Thread HiloEscuchar = new Thread(new ThreadStart(HILO_EscucharConexiones));
            HiloEscuchar.Start();
        }

        // Método que se ejecutará en otro hilo y que sirve para qu socket escuche peticiones
        // de conexión entrantes
        private void HILO_EscucharConexiones()
        {
            // Creo un nuevo Socket para escuchar las conexiones entrantes. (Elijo un puerto que no este en uso).
            SocketEscucha = new TcpListener(30000);

            // Ejecuto este hilo hasta que se haya establecido una conexión
            bool ConexionEstablecida = false;
            while (ConexionEstablecida == false)
            {
                // Cuando haya una conexión entrante la acepto y creo un nuevo hilo que reciba los datos y los 
                // transforme en una imagen.
                if (SocketEscucha.Pending() == true)
                {
                    SocketRecepcion = SocketEscucha.AcceptSocket();
                    Thread HiloRecibirDatos = new Thread(new ThreadStart(HILO_RecibirDatos));
                    HiloRecibirDatos.Start();
                    ConexionEstablecida = true;
                }
                else
                    Thread.Sleep(50);
            }
        }
        // Método que se ejecuta en otro hilo y recibe los datos.
        private void HILO_RecibirDatos()
        {
            // Recibo la longitud del flujo de datos que compone la imagen
            while(SocketRecepcion.Available < 4)
            {
                Thread.Sleep(50);
            }
            int Tamaño;
            byte[] DatosRecibidos;

            // Redimensiono el arreglo de bytes al tamaño de flujo
            DatosRecibidos = new byte[4];
            SocketRecepcion.Receive(DatosRecibidos, 4, SocketFlags.None);
            Tamaño = BitConverter.ToInt32(DatosRecibidos, 0);

            // Espero a que la cantidad de bytes en el buffer llegue al tamaño total del flujo.
            while (SocketRecepcion.Available < Tamaño)
                Thread.Sleep(50);

            // Leo la cantidad de bytes según el tamaño del flujo
            DatosRecibidos = new byte[Tamaño];
            SocketRecepcion.Receive(DatosRecibidos, Tamaño, SocketFlags.None);

            // Transforma el flujo en una imagen
            MemoryStream FlujoImagen = new MemoryStream();
            FlujoImagen.Write(DatosRecibidos, 0, Tamaño);
        }

        private void casillaClick(object sender, EventArgs e)
        {
            int columna;
            int fila;
            int nCol, nFila;

            nCol = getNumColumnas();
            nFila = getNumFilas();

            columna = ((Casilla)sender).getCol();
            fila = ((Casilla)sender).getFila();

            if (fila <= getNumFilas())
            {
                pintarFicha(columna);
                MovFicha();
            }

            // Creo un Socket con las propiedades de conexión correspondientes.
            SocketEnvio = new TcpClient();

            // Intento crear la conexión
            SocketEnvio.Connect(System.Net.IPAddress.Any, 30000);

            // Transformo la imagen en un flujo de bytes.
            byte[] DatosImagen;
            MemoryStream FlujoImagen = new MemoryStream();
            DatosImagen = ObjToByteArray(tablero);
            


        }
        private static byte[] ObjToByteArray(object Casilla)
        {
            if (Casilla == null)
            {
                return null;
            }

            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, Casilla);
                return ms.ToArray();
            }
        }

        private void pintarFicha(int col)
        {

            if (jugador == 1)
            {
                for (int i = 0; i < getNumFilas(); i++)
                {
                    if (tablero[i, col].vacia)
                    {
                        if (((i - 1) < getNumFilas() - 1) && (i > 0))
                        {
                            tablero[i - 1, col].Image = Properties.Resources.Vacia;
                            tablero[i - 1, col].SizeMode = PictureBoxSizeMode.StretchImage;

                        }

                        tablero[i, col].Image = Properties.Resources.fichaRoja;
                        tablero[i, col].SizeMode = PictureBoxSizeMode.StretchImage;
                        tablero[i, col].jugador = 1;
                        tablero[i, col].roja = true;



                        if (i == (getNumFilas() - 1))
                        {
                            tablero[i, col].Image = Properties.Resources.fichaRoja;
                            tablero[i, col].SizeMode = PictureBoxSizeMode.StretchImage;
                            tablero[i, col].roja = true;
                            tablero[i, col].jugador = 1;
                            tablero[i, col].vacia = false;
                            if (ganar(i, col, 1) == 1)
                            {
                               /* _panelP1.Visible = true;
                                labelP1.Text = "PLAYER 1 WINS!";
                                _panelP1.BringToFront();*/
                            }
                        }
                    }

                    else if (i > 0)
                    {

                        tablero[i - 1, col].vacia = false;
                        tablero[i - 1, col].Image = Properties.Resources.fichaRoja;
                        tablero[i - 1, col].SizeMode = PictureBoxSizeMode.StretchImage;
                        tablero[i - 1, col].roja = true;
                        tablero[i - 1, col].jugador = 1;
                        if (ganar(i - 1, col, 1) == 1)
                        {
                           /* _panelP1.Visible = true;
                            labelP1.Text = "PLAYER 1 WINS!";
                            _panelP1.BringToFront();*/
                        }
                        break;
                    }

                    Task.Delay(3000);
                    this.Refresh();

                }


                jugador = 2;
            }
            else
            {
                for (int i = 0; i < getNumFilas(); i++)
                {
                    if (tablero[i, col].vacia)
                    {
                        if (((i - 1) < getNumFilas() - 1) && (i > 0))
                        {
                            tablero[i - 1, col].Image = Properties.Resources.Vacia;
                            tablero[i - 1, col].SizeMode = PictureBoxSizeMode.StretchImage;

                        }

                        tablero[i, col].Image = Properties.Resources.fichaAmarilla;
                        tablero[i, col].SizeMode = PictureBoxSizeMode.StretchImage;
                        tablero[i, col].jugador = 2;
                        tablero[i, col].amarilla = true;


                        if (i == (getNumFilas() - 1))
                        {
                            tablero[i, col].vacia = false;
                            tablero[i, col].jugador = 2;
                            if (ganar(i, col, 2) == 1)
                            {
                               /* panelP1.Visible = true;
                                labelP1.Text = "PLAYER 2 WINS!";
                                _panelP1.BringToFront();*/
                            }

                        }
                    }

                    else if (i > 0)
                    {
                        tablero[i - 1, col].vacia = false;
                        tablero[i - 1, col].jugador = 2;
                        if (ganar(i - 1, col, 2) == 1)
                        {
                           /* panelP1.Visible = true;
                            labelP1.Text = "PLAYER 2 WINS!";
                            _panelP1.BringToFront();*/
                        }

                        break;
                    }

                    Task.Delay(3000);
                    this.Refresh();


                }

                jugador = 1;
            }

        }


        private void MovFicha()
        {
            movimientos++;
        }

        public int ganar(int fila_actual, int columna_actual, int jugador)
        {
            //bool resultado = false;
            int contador_ganar = 4;
            int contador = 0;

            //horizontal -
            for (int f = 0; f < getNumFilas() - 1; f++)
            {
                for (int c = 0; c < getNumColumnas() - 1; c++)
                {
                    if (!tablero[f, c].vacia && jugador == tablero[f, c].jugador && jugador == tablero[f, c + 1].jugador && jugador == tablero[f, c + 2].jugador && jugador == tablero[f, c + 3].jugador)
                        contador = 4;
                    /*
                        if (c <= getNumColumnas()-4 && !tablero[f,c].vacia && tablero[f, c].jugador == tablero[f, c + 1].jugador && tablero[f, c].jugador == tablero[f, c + 2].jugador && tablero[f, c].jugador == tablero[f, c + 3].jugador)
                            contador=4;
                    
                   
                        else if (!tablero[f, c].vacia && tablero[f, c].jugador == tablero[f, c - 1].jugador && tablero[f, c].jugador == tablero[f, c - 2].jugador && tablero[f, c].jugador == tablero[f, c - 3].jugador)
                            contador=4;
                    */
                }

                if (contador == contador_ganar)
                    return 1;
            }
            /*for (int c = 0; c <= getNumColumnas() - 1; c++)
            {
                if(!tablero[fila_actual,c].vacia)
                {
                    if (fila_actual >= getNumFilas())
                    {
                        if (jugador == tablero[fila_actual, c].jugador && jugador == tablero[fila_actual, c + 1].jugador && jugador == tablero[fila_actual, c + 2].jugador && jugador == tablero[fila_actual, c + 3].jugador)
                        {
                            contador = 4;
                        }
                    }
                    else if (c >= getNumColumnas()-4)
                    {

                        if (jugador == tablero[fila_actual, c].jugador && jugador == tablero[fila_actual, c - 1].jugador && jugador == tablero[fila_actual, c - 2].jugador && jugador == tablero[fila_actual, c - 3].jugador)
                        {
                            contador = 4;
                        }
                    }
                }
              
                if (contador == contador_ganar)
                    return 1;
            } 
            */

            //vertical |
            for (int f = fila_actual; f <= getNumFilas() - 1; f++)
            {

                if (!tablero[f, columna_actual].vacia && jugador == tablero[f, columna_actual].jugador)
                {
                    contador++;
                }
                else
                    break;

                if (contador == contador_ganar)
                    return 1;

            }


            //diagonal ascendente /    
            for (int f = 0; f <= getNumFilas() - 1; f++)
            {
                for (int c = 0; c <= getNumColumnas() - 1; c++)
                {
                    if (!tablero[f, c].vacia)
                    {
                        if (f <= getNumFilas() - 4 && c >= getNumColumnas() - 4 && jugador == tablero[f, c].jugador && jugador == tablero[f + 1, c - 1].jugador && jugador == tablero[f + 2, c - 2].jugador && jugador == tablero[f + 3, c - 3].jugador)
                        {
                            contador = 4;
                        }
                        else if (f >= getNumFilas() && jugador == tablero[f, c].jugador && jugador == tablero[f - 1, c + 1].jugador && jugador == tablero[f - 2, c + 2].jugador && jugador == tablero[f - 3, c + 3].jugador)
                        {
                            contador = 4;
                        }

                    }
                    if (contador == contador_ganar)
                        return 1;
                }
            }

            //diagonal decendente \
            for (int f = 0; f <= getNumFilas() - 1; f++)
            {
                for (int c = 0; c <= getNumColumnas() - 1; c++)
                {
                    if (!tablero[f, c].vacia)
                    {
                        if (c >= getNumColumnas() - 4 && f >= getNumFilas() - 4 && jugador == tablero[f, c].jugador && jugador == tablero[f - 1, c - 1].jugador && jugador == tablero[f - 2, c - 2].jugador && jugador == tablero[f - 3, c - 3].jugador)
                            contador = 4;
                        else if (c <= getNumColumnas() - 4 && f <= getNumFilas() - 4 && jugador == tablero[f, c].jugador && jugador == tablero[f + 1, c + 1].jugador && jugador == tablero[f + 2, c + 2].jugador && jugador == tablero[f + 3, c + 3].jugador)
                            contador = 4;
                    }

                    if (contador == contador_ganar)
                        return 1;

                }
            }
            return 0;

        }

        public void crearTablero(int fila, int col)
        {
            tablero = new Casilla[fila, col];
            for (int f = 0; f < fila; f++)
                for (int c = 0; c < col; c++)
                {
                    tablero[f, c] = new Casilla(panelTablero);
                    tablero[f, c].setFila(f);
                    tablero[f, c].setCol(c);
                    tablero[f, c].posicion(60 * f, 60 * c);
                    tablero[f, c].dimension(60, 60);
                    tablero[f, c].Image = Properties.Resources.Vacia;
                    tablero[f, c].SizeMode = PictureBoxSizeMode.StretchImage;
                    tablero[f, c].Visible = true;
                    tablero[f, c].BringToFront();
                    tablero[f, c].Click += casillaClick;
                }
            this.Refresh();
        }

    }
}
